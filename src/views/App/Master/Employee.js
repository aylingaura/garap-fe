import React, { useState, useEffect } from 'react'
import axios from 'axios'
import dateFormat from 'dateformat'
import toast, { Toaster } from 'react-hot-toast'
import moment from 'moment'
import DatePicker from "react-datepicker"
import Select from 'react-select'

import { useContext } from '../../../components/Context'
import NotifError from '../Notification/Error'
import Loader from '../../../components/Loader_.js'
import NoPhoto from '../../../assets/img/no-photo.png'

// formData.photo != '' || !formData.photo || formData.photo != null ? formData.photo : 

import { list_ as userTypeList } from './UserType'

import {
    CDataTable,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader
} from '@coreui/react'
import {
    FaRegDotCircle,
    FaRegEdit,
    FaTrash,
    FaUserTag,
    FaBan,
    FaPaperPlane,
    FaCamera
} from 'react-icons/fa'

const fields = [
    {
        key: 'no',
        _style: { width: '5%' },
    },
    {
        key: 'nip',
    },
    {
        key: 'name',
    },
    {
        key: 'is_active',
        label: 'Status',
    },
    {
        key: 'edit',
        label: '',
        _style: { width: '1%' },
    },
    {
        key: 'delete',
        label: '',
        _style: { width: '1%' },
    }
]

const Employee = () => {

    // notification
    const [loader, setLoader]             = useState(false)
    const [errorMessage, setErrorMessage] = useState('') // error message for notification error from api

    // data form
    const [formData, setFormData] = useState([]) // data from input form
    const [errorMsg, setErrorMsg] = useState([]) // error message for input form
    const [dropdownUserType, setDropdownUserType] = useState([])
    const [userTypeValue, setUserTypeValue] = useState([])
    const [checkboxIsActive, setCheckboxIsActive] = useState(false)
    const [isEdit, setIsEdit] = useState(false)
    const [tglLahir, setTglLahir] = useState(new Date()) // input date

    // data table
    const [data, setData] = useState([])

    // modal
    const [modalTitle, showModalTitle]   = useState('')
    const [modalDelMsg, showModalDelMsg] = useState('') // message for modal delete
    const [modalInput, showModalInput]   = useState(false)
    const [modalDelete, showModalDelete] = useState(false) // modal delete

    // context (global var/func)
    const { headers, checkLogin } = useContext()
    const config  = { headers: headers }

    const subUrl = 'master/employee/'

    // loader before axios send
    axios.interceptors.request.use(config => {
        setLoader(true)
        return config;
    }, error => {
        return Promise.reject(error);
    });

    // list table
    const getList = () => {

        checkLogin()

        const url    = process.env.REACT_APP_ROOT_URL + subUrl
        const config = {
          headers: headers
        }

        axios.get(url, config)
        .then( res => {
            setData(res.data.data)
        })
        .catch(err => {
            setErrorMessage(err.response.status + ' - ' + err.response.statusText)
        })
        .then(() => {
            setLoader(false)
        })
    }

    // load moadl input add/edit
    const loadModalInput = (id_employee) => {

        // reset form
        setFormData([])
        setErrorMsg([])
        setTglLahir()
        setUserTypeValue({
            label : '', value: ''
        })
        dropdownUserType_()

        // edit form
        if (id_employee) {
            setIsEdit(true)
            showModalInput(!modalInput)
            showModalTitle('Edit Data')
            detailData(id_employee)
        }

        // add form
        else {
            setIsEdit(false)
            showModalTitle('Add Data')
            showModalInput(!modalInput)
            setCheckboxIsActive(false)
        }

    }

    // show modal delete
    const loadModalDelete = (id_employee, msg) => {

        showModalDelete(!modalDelete)
        showModalDelMsg(msg)
        setFormData({
            'id_employee' : id_employee
        })
    }

    // dropdown user type from UserType.js
    const dropdownUserType_ = () => {

        setDropdownUserType([])
        
        userTypeList().then( res => {
            res.data.data.forEach((v,k)=>{
                setDropdownUserType(dropdownUserType => [...dropdownUserType, {value : v.id_user_type, label : (v.name ? v.name : '-') }])
            })
        })
        .catch(err => {
            setErrorMessage(err.response.status + ' - ' + err.response.statusText)
        })
        .then(() => {
            setLoader(false)
        })
    }

    // handle input select option user type
    const handleSelectUserType = (data) => {
        setUserTypeValue({
            label : data.label, value: data.value
        })

        setFormData({
            ...formData,
            'id_user_type' : data.value,
        })
    }

    // handle input
    const handleFormInput = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        })
    }

    // handle input file photo
    const handleFile = (e) => {

        var file = e.target.files[0];
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);

        reader.onloadend = function (e) {
            setFormData({
                ...formData,
                "photo" : reader.result,
            })
        }.bind(this);

    }

    // handle input date
    const handleInputDate = (name, date) => {

        setTglLahir(date)
        setFormData({
            ...formData,
            [name]: date,
        })
    }

    // handle checkbox is active
    const handleCheckboxIsActive = () => {

        setCheckboxIsActive(!checkboxIsActive)
        setFormData({
            ...formData,
            "is_active" : !checkboxIsActive,
        })
    }

    // submit form
    const submitForm = () => {

        // update data
        if(formData.id_employee){
            updateData(formData.id_employee)
        }

        // input data
        else{
            addData()
        }

    }

    // add data
    const addData = () => {

        const url = process.env.REACT_APP_ROOT_URL + subUrl
        axios.post(url, formData, config)
        .then( res => {

            getList()
            showModalInput(!modalInput)
            toast.success('Omedetō.. Success add data')

        })
        .catch(error => {

            let errorMsgTemp = {}

            Object.keys(error.response.data.message).forEach((val) => {
                errorMsgTemp[[val]] = error.response.data.message[val][0]
            })

            setErrorMsg(errorMsgTemp)

        })
        .then(() => {
            setLoader(false)
        })
    }

    // detail data
    const detailData = (id_employee) => {
        const urlDetail = process.env.REACT_APP_ROOT_URL + subUrl + id_employee

        axios.get(urlDetail, config)
        .then( res => {
            setFormData(res.data.data)
            setCheckboxIsActive(res.data.data.is_active)
            setTglLahir(moment(res.data.data.tanggal_lahir).toDate())
            setUserTypeValue({
                label : res.data.data.user_type.name, value: res.data.data.id_user_type
            })
        })
        .catch(error => {
            toast.error(error.message)
        })
        .then(() => {
            setLoader(false)
        })
    }

    // update data
    const updateData = (id_employee) => {
        
        const url = process.env.REACT_APP_ROOT_URL + subUrl + id_employee

        axios.put(url, formData, config)
        .then( res => {

            getList()
            showModalInput(!modalInput)
            toast.success('Omedetō.. Success update data')

        })
        .catch(error => {
            console.log(error)
            let errorMsgTemp = {}

            Object.keys(error.response.data.message).forEach((val) => {
                errorMsgTemp[[val]] = error.response.data.message[val][0]
            })

            setErrorMsg(errorMsgTemp)

        })
        .then(() => {
            setLoader(false)
        })
    }

    // delete data
    const deleteData = (msg) => {

        const url = process.env.REACT_APP_ROOT_URL + subUrl + formData.id_employee

        axios.delete(url, config)
        .then( res => {
            toast.success('Omedetō.. ' + msg + ' Success Deleted')
        })
        .catch(error => {
            toast.error(error.message)
        })
        .then(() => {
            getList()
            showModalDelete(!modalDelete)
            setLoader(false)
        })

    }


    useEffect(() => {
        getList()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    
    return (
        <>
            <NotifError message={errorMessage}/>
            <Loader isActive={loader} />
            <Toaster position="top-center"
                toastOptions = {{
                    className: 'p-2 px-3',
                    style: {
                        fontSize: '15px',
                        boxShadow: '1px 3px 3px 1px #bccad9'
                    }
                }}
            />

            <div className="px-3">

                <div className="row">
                    <div className="col-md-12">
                        <h3 className="text-black-50"><b><FaUserTag className="mr-2 text-primary-color" />Employee</b></h3>
                        <h6 className="text-black-50 font-weight-bold">Employee</h6>
                    </div>
                </div>

                <div className="row mt-4 px-5 py-4 mb-5 c-box-shadow rounded-3">
                    <div className="col-md-12 mt-2 text-right">
                        <button className="btn btn-outline-info btn-sm col-md-2" onClick={() => loadModalInput(null)}>
                            <FaRegDotCircle className="mr-2 mb-1"/>Tambah Data
                        </button>
                    </div>
                    <div className="col-md-12">
                        <CDataTable
                            size="sm"
                            items={data}
                            fields={fields}
                            tableFilter={{ 'placeholder': 'search here ...'}}
                            hover
                            striped
                            pagination
                            itemsPerPage={10}
                            scopedSlots = {{
                                'no': (item, index) => {
                                    return (
                                        <td className={ item.is_active ? '' : 'text-black-25' }>{index + 1}</td>
                                    )
                                },
                                'nip': (item, index) => {
                                    return (
                                        <td className={ item.is_active ? '' : 'text-black-25' }>{ item.nip }</td>
                                    )
                                },
                                'name': (item, index) => {
                                    return (
                                        <td className={ item.is_active ? '' : 'text-black-25' }>{ item.name }</td>
                                    )
                                },
                                'is_active': (item, index) => {
                                    return (
                                        <td className={ item.is_active ? 'text-primary-color-light' : 'text-danger' }>{ item.is_active ? 'active' : 'non active' }</td>
                                    )
                                },
                                'edit': (item, index) => { return (
                                    <td className="px-1">
                                        <button className={ ( !item.is_edit ? 'disabled btn-outline-secondary' : 'btn-outline-info' ) + ' btn btn-sm' } onClick={ () => !item.is_edit ? toast.error('access denied') : loadModalInput(item.id_employee) }>
                                            <FaRegEdit className="mt--04" />
                                        </button>
                                    </td>
                                )},
                                'delete': (item, index) => { return (
                                    <td className="px-1">
                                        <button className={ ( !item.is_edit ? 'disabled btn-outline-secondary' : 'btn-outline-danger' ) + ' btn btn-sm' } onClick={ () => !item.is_edit ? toast.error('access denied') : loadModalDelete(item.id_employee, item.name) }>
                                            <FaTrash className="mt--04" />
                                        </button>
                                    </td>
                                )}
                            }}
                        />
                    </div>
                </div>


                {/* modal input */}
                <CModal show={modalInput} onClose={() => showModalInput(!modalInput)} size="lg">
                    <CModalHeader className="px-5" style={{ borderBottom: "none" }} closeButton>
                        <span className="font-weight-bold">{modalTitle} Employee</span>
                    </CModalHeader>

                    <CModalBody className="px-5">

                        <div className="row">

                            <div className={ (isEdit ? "col-md-9 pr-2" : "col-md-12") }>
                                
                                <div className="row mt-1">
                                    <div className="col-md-3 mt-1">
                                        <span>Photo</span>
                                    </div>
                                    <div className="col-md-9">
                                        <img alt="foto profile" src={formData.photo ? formData.photo : NoPhoto} style={{ padding: formData.photo ? 15 : 70, height: 200, width: 200, borderRadius: 10, border: '1px solid rgba(0, 0, 0, 0.1)' }} />
                                    </div>
                                </div>
                                <div className="row mt-1">
                                    <div className="col-md-9 offset-3">
                                        <label for="btn-photo" style={{ fontSize: 18 }}><FaCamera/></label>
                                        <input type="file" id="btn-photo" className="d-none" onChange={handleFile} name="photo" accept=".jpg,.jpeg,.png"/>
                                        <small className="text-danger"> <i>{errorMsg.photo ? errorMsg.photo : ''}</i> </small>
                                    </div>
                                </div>

                                <div className="row mt-2">
                                    <div className="col-md-3 mt-1">
                                        <span>User Type</span>
                                    </div>
                                    <div className="col-md-9">
                                        <Select options={dropdownUserType} value={userTypeValue} name="id_user_type" onChange={handleSelectUserType}/>
                                        <small className="text-danger"> <i>{errorMsg.id_user_type ? errorMsg.id_user_type : ''}</i> </small>
                                    </div>
                                </div>
                                <div className="row mt-1">
                                    <div className="col-md-3 mt-1">
                                        <span>Name</span>
                                    </div>
                                    <div className="col-md-9">
                                        <input value={formData.name ? formData.name : ''} onChange={handleFormInput} className="form-control" name="name"/>
                                        <small className="text-danger"> <i>{errorMsg.name ? errorMsg.name : ''}</i> </small>
                                    </div>
                                </div>
                                <div className="row mt-1">
                                    <div className="col-md-3 mt-1">
                                        <span>Tempat Lahir</span>
                                    </div>
                                    <div className="col-md-9">
                                        <input value={formData.tempat_lahir ? formData.tempat_lahir : ''} onChange={handleFormInput} className="form-control" name="tempat_lahir"/>
                                        <small className="text-danger"> <i>{errorMsg.tempat_lahir ? errorMsg.tempat_lahir : ''}</i> </small>
                                    </div>
                                </div>
                                <div className="row mt-1">
                                    <div className="col-md-3 mt-4">
                                        <span>Tanggal Lahir</span>
                                    </div>
                                    <div className="col-md-9">
                                        <small className="text-black-50 font-italic">format date : <b>dd-mm-yyyy (19-04-1999)</b></small>
                                        <DatePicker dateFormat="dd-MM-yyyy" selected={tglLahir} className="form-control" onChange={(date) => handleInputDate('tanggal_lahir', date)} />
                                        <small className="text-danger"> <i>{errorMsg.tanggal_lahir ? errorMsg.tanggal_lahir : ''}</i> </small>
                                    </div>
                                </div>
                                <div className="row mt-1">
                                    <div className="col-md-3 mt-1">
                                        <span>Alamat</span>
                                    </div>
                                    <div className="col-md-9">
                                        <textarea value={formData.alamat ? formData.alamat : ''} onChange={handleFormInput} className="form-control" name="alamat"></textarea>
                                        <small className="text-danger"> <i>{errorMsg.alamat ? errorMsg.alamat : ''}</i> </small>
                                    </div>
                                </div>
                                <div className="row mt-1">
                                    <div className="col-md-3 mt-1">
                                        <span>Email</span>
                                    </div>
                                    <div className="col-md-9">
                                        <input value={formData.email ? formData.email : ''} onChange={handleFormInput} className="form-control" name="email"/>
                                        <small className="text-danger"> <i>{errorMsg.email ? errorMsg.email : ''}</i> </small>
                                    </div>
                                </div>
                                <div className="row mt-1">
                                    <div className="col-md-3 mt-1">
                                        <span>Kontak</span>
                                    </div>
                                    <div className="col-md-9">
                                        <input value={formData.phone ? formData.phone : ''} onChange={handleFormInput} className="form-control" name="phone"/>
                                        <small className="text-danger"> <i>{errorMsg.phone ? errorMsg.phone : ''}</i> </small>
                                    </div>
                                </div>
                                <div className="row mt-1">
                                    <div className="col-md-3">
                                        <span>Active</span>
                                    </div>
                                    <div className="col-md-1">
                                        <div className="checkbox">
                                            <input type="checkbox" id="is_active" name="is_active" onChange={handleCheckboxIsActive} checked={ checkboxIsActive ? "checked" : '' }/>
                                            <label htmlFor="is_active"><span></span></label>
                                        </div>
                                        <small className="text-danger"> <i>{errorMsg.is_active ? errorMsg.is_active : ''}</i> </small>
                                    </div>
                                </div>
                                <div className="row mt-4">
                                    <div className="col-md-12 text-right">
                                        <button type="button" className="btn btn-sm btn-default w-100-p mx-1" onClick={() => showModalInput(!modalInput)}><FaBan className="mr-2" />Cancel</button>
                                        <button type="button" className="btn btn-sm btn-success w-100-p mx-1" onClick={() => submitForm()}><FaPaperPlane className="mr-2" />{isEdit ? "Edit" : "Add"}</button>
                                    </div>
                                </div>

                            </div>

                            <div className={ isEdit ? "col-md-3 pl-1" : "d-none" }>
                                <div className="card">
                                    <div className="card-body pb-1">
                                        <div className="row p-0">
                                            <div className="col-md-12 py-0">
                                                <p className="text-black-50 f-10">Created At : </p>
                                                <p className="f-10 mt--15 text-primary-color font-weight-bold">{formData.created_at ? dateFormat(formData.created_at, "mm/dd/yyyy HH:MM:ss") : '-'}</p>
                                            </div>
                                            <div className="col-md-12 py-0 mt--13">
                                                <p className="text-black-50 f-10">Created By : </p>
                                                <p className="f-10 mt--15 text-primary-color font-weight-bold">{formData.created_by ? formData.created_by : '-'}</p>
                                            </div>
                                            <div className="col-md-12 py-0">
                                                <p className="text-black-50 f-10">Updated At : </p>
                                                <p className="f-10 mt--15 text-primary-color font-weight-bold">{formData.updated_at ? dateFormat(formData.updated_at, "mm/dd/yyyy HH:MM:ss") : '-'}</p>
                                            </div>
                                            <div className="col-md-12 py-0 mt--13">
                                                <p className="text-black-50 f-10">Updated By : </p>
                                                <p className="f-10 mt--15 text-primary-color font-weight-bold">{formData.updated_by ? formData.updated_by : '-'}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </CModalBody>
                </CModal>

                {/* modal delete */}
                <CModal show={modalDelete} onClose={() => showModalDelete(!modalDelete)}>
                    <CModalHeader style={{ borderBottom: "none" }} closeButton>
                    </CModalHeader>
                    <CModalBody>
                        <div className="row mb-3">
                            <div className="col-md-12 mt-1 text-center">
                                <span>Delete <b>{modalDelMsg}</b> ?</span>
                            </div>
                        </div>
                    </CModalBody>
                    <CModalFooter className="justify-content-center" style={{ borderTop: "none" }}>
                        <button type="button" className="btn btn-sm btn-default w-100-p" onClick={() => deleteData(modalDelMsg)}><FaTrash className="mr-2 mt--04" />Delete</button>
                        <button type="button" className="btn btn-sm btn-success w-100-p" onClick={() => showModalDelete(!modalDelete)}><FaBan className="mr-2 mt--04" />Cancel</button>
                    </CModalFooter>
                </CModal>

            </div>
        </>
    )
}

export default Employee
