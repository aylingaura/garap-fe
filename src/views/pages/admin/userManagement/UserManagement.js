import React, { useState, useEffect } from 'react'
import axios from 'axios'
import toast, { Toaster } from 'react-hot-toast'
import Select from 'react-select'
import { useSelector, useDispatch } from 'react-redux'

import { useContext } from '../../../../components/Context'
import NotifError from '../../../App/Notification/Error'
import Loader from '../../../../components/Loader_.js'

import {
    CDataTable,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader
} from '@coreui/react'
import {
    FaRegDotCircle,
    FaRegEdit,
    FaTrash,
    FaUserTag,
    FaBan,
    FaPaperPlane,
    FaKey
} from 'react-icons/fa'

const fields = [
    {
        key: 'no',
        _style: { width: '5%' },
    },
    {
        key: 'username',
        label: 'Username',
    },
    {
        key: 'user_type',
        label: 'User Type',
    },
    {
        key: 'edit',
        label: '',
        _style: { width: '1%' },
    },
    {
        key: 'delete',
        label: '',
        _style: { width: '1%' },
    }
]

const UserManagement = () => {

    const headers = useSelector(state => state.headers)

    // notification
    const [loader, setLoader] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')

    // data form
    const [dropdownUserType, setDropdownUserType] = useState([])
    const [userTypeValue, setUserTypeValue] = useState([])
    const [formData, setFormData] = useState([])
    const [errorMsg, setErrorMsg] = useState([])
    const [secPass, setSecPass] = useState(false)

    // data table
    const [data, setData] = useState([])

    // modal
    const [modalTitle, showModalTitle] = useState('')
    const [modalDelMsg, showModalDelMsg] = useState('')
    const [modalInput, showModalInput] = useState(false)
    const [modalDelete, showModalDelete] = useState(false)
    const [modalChPass, showModalChPass] = useState(false)

    // context (global var/func)
    const { checkLogin } = useContext()
    const config = headers

    // loader before axios send
    axios.interceptors.request.use(config => {
        setLoader(true)
        return config;
    }, error => {
        return Promise.reject(error);
    });


    // list table
    const getList = () => {

        checkLogin()

        const url = process.env.REACT_APP_ROOT_URL + "user-management/user"
        axios.get(url, config)
            .then(res => {
                setData(res.data.data)
            })
            .catch(err => {
                setErrorMessage(err.response.status + ' - ' + err.response.statusText)
            })
            .then(() => {
                setLoader(false)
            })
    }

    // load moadl input add/edit
    const loadModalInput = (id) => {

        // reset form
        setFormData([])
        setErrorMsg([])
        setUserTypeValue({
            label: '', value: ''
        })
        dropdownUserType_()

        // edit form
        if (id) {
            setLoader(false)
            setSecPass(false)
            showModalInput(!modalInput)
            showModalTitle('Edit Data')
            detailData(id)
        }

        // add form
        else {
            showModalTitle('Add Data')
            setSecPass(true)
            showModalInput(!modalInput)
        }

    }

    // load modal change password
    const loadModalChPass = () => {
        showModalChPass(!modalChPass)
        showModalInput(!modalInput)
    }

    // show modal delete
    const loadModalDelete = (id, msg) => {

        showModalDelete(!modalDelete)
        showModalDelMsg(msg)
        setFormData({
            'id': id
        })
    }

    // handle input
    const handleFormInput = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        })
    }

    // handle input select option user type
    const handleSelectUserType = (data) => {
        setUserTypeValue({
            label: data.label, value: data.value
        })

        setFormData({
            ...formData,
            'id_user_type': data.value,
            'id_user': '',
        })

    }


    // submit form
    const submitForm = () => {

        // update data
        if (formData.id) {
            updateData(formData.id)
        }

        // input data
        else {
            addData()
        }

    }

    // submit change password
    const submitChPass = () => {
        const dataPass = {
            "password": formData.password,
            "confirm_password": formData.confirm_password
        }
        const urlChPass = process.env.REACT_APP_ROOT_URL + 'user-management/user/reset-password/' + formData.id

        axios.put(urlChPass, dataPass, config)
            .then(res => {

                getList()
                showModalInput(!modalInput)
                showModalChPass(!modalChPass)
                toast.success('Omedetō.. Success update password')

            })
            .catch(error => {

                let errorMsgTemp = {}

                Object.keys(error.response.data.message).forEach((val) => {
                    errorMsgTemp[[val]] = error.response.data.message[val][0]
                })

                setErrorMsg(errorMsgTemp)

            })
            .then(() => {
                setLoader(false)
            })
    }

    // select option dropdown user type
    const dropdownUserType_ = () => {
        const urlDropdown = process.env.REACT_APP_ROOT_URL + "master/user-type"
        setDropdownUserType([])

        axios.get(urlDropdown, config)
            .then(res => {
                res.data.data.forEach((v, k) => {
                    setDropdownUserType(dropdownUserType => [...dropdownUserType, { value: v.id_user_type, label: (v.name ? v.name : '-') }])
                })
            })
            .catch(err => {
                setErrorMessage(err.response.status + ' - ' + err.response.statusText)
            })
            .then(() => {
                setLoader(false)
            })
    }

    // add data
    const addData = () => {
        const data = {
            "username": formData.username,
            "id_user_type": formData.id_user_type,
            "password": formData.password,
            "confirm_password": formData.confirm_password,
        }

        const url = process.env.REACT_APP_ROOT_URL + 'user-management/user'

        axios.post(url, data, config)
            .then(res => {

                getList()
                showModalInput(!modalInput)
                toast.success('Omedetō.. Success add data')

            })
            .catch(error => {

                let errorMsgTemp = {}

                Object.keys(error.response.data.message).forEach((val) => {
                    errorMsgTemp[[val]] = error.response.data.message[val][0]
                })

                setErrorMsg(errorMsgTemp)

            })
            .then(() => {
                setLoader(false)
            })
    }

    // detail data
    const detailData = (id) => {
        const urlDetail = process.env.REACT_APP_ROOT_URL + 'user-management/user/' + id

        axios.get(urlDetail, config)
            .then(res => {

                setFormData(res.data.data)
                setUserTypeValue({
                    label: res.data.data.user_type.name, value: res.data.data.id_user_type
                })
            })
            .catch(error => {
                toast.error(error.message)
            })
            .then(() => {

            })
    }

    // update data
    const updateData = (id) => {
        const data = {
            "username": formData.username,
            "id_user_type": formData.id_user_type,
        }
        const url = process.env.REACT_APP_ROOT_URL + 'user-management/user/' + id

        axios.put(url, data, config)
            .then(res => {

                getList()
                showModalInput(!modalInput)
                toast.success('Omedetō.. Success update data')

            })
            .catch(error => {

                let errorMsgTemp = {}

                Object.keys(error.response.data.message).forEach((val) => {
                    errorMsgTemp[[val]] = error.response.data.message[val][0]
                })

                setErrorMsg(errorMsgTemp)

            })
            .then(() => {
                setLoader(false)
            })
    }

    // delete data
    const deleteData = (msg) => {

        const url = process.env.REACT_APP_ROOT_URL + 'user-management/user/' + formData.id

        axios.delete(url, config)
            .then(res => {
                toast.success('Omedetō.. ' + msg + ' Success Deleted')
            })
            .catch(error => {
                toast.error(error.message)
            })
            .then(() => {
                getList()
                showModalDelete(!modalDelete)
                setLoader(false)
            })

    }


    useEffect(() => {
        getList()
    }, [])

    return (
        <>
            <NotifError message={errorMessage} />
            <Loader isActive={loader} />
            <Toaster position="top-center"
                toastOptions={{
                    className: 'p-2 px-3',
                    style: {
                        fontSize: '15px',
                        boxShadow: '1px 3px 3px 1px #bccad9'
                    }
                }}
            />

            <div className="px-3">

                <div className="row">
                    <div className="col-md-12">
                        <h3 className="text-black-50"><b><FaUserTag className="mr-2 text-primary-color" />User</b></h3>
                        <h6 className="text-black-50 font-weight-bold">List user who can access system</h6>
                    </div>
                </div>

                <div className="row mt-4 px-5 py-4 mb-5 c-box-shadow rounded-3">
                    <div className="col-md-12 mt-2 text-right">
                        <button className="btn btn-outline-info btn-sm col-md-2" onClick={() => loadModalInput(null)}>
                            <FaRegDotCircle className="mr-2 mb-1" />Tambah Data
                        </button>
                    </div>
                    <div className="col-md-12">
                        <CDataTable
                            size="sm"
                            items={data}
                            fields={fields}
                            tableFilter={{ 'placeholder': 'search here ...' }}
                            hover
                            striped
                            pagination
                            itemsPerPage={10}
                            scopedSlots={{
                                'no': (item, index) => {
                                    return (
                                        <td>{index + 1}</td>
                                    )
                                },
                                'username': (item, index) => {
                                    return (
                                        <td>{item.username ? item.username : '-'}</td>
                                    )
                                },
                                'user_type': (item, index) => {
                                    return (
                                        <td>{item.user_type ? item.user_type.name : '-'}</td>
                                    )
                                },
                                'edit': (item, index) => {
                                    return (
                                        <td className="px-1">
                                            <button className={'btn-outline-info btn btn-sm'} onClick={() => loadModalInput(item.id)}>
                                                <FaRegEdit className="mt--04" />
                                            </button>
                                        </td>
                                    )
                                },
                                'delete': (item, index) => {
                                    return (
                                        <td className="px-1">
                                            <button className={'btn-outline-danger btn btn-sm'} onClick={() => loadModalDelete(item.id, item.username)}>
                                                <FaTrash className="mt--04" />
                                            </button>
                                        </td>
                                    )
                                }
                            }}
                        />
                    </div>
                </div>


                {/* modal input */}
                <CModal show={modalInput} onClose={() => showModalInput(!modalInput)} size="lg">
                    <CModalHeader className="pl-5" style={{ borderBottom: "none" }} closeButton>
                        <span className="font-weight-bold">{modalTitle} User</span>
                    </CModalHeader>

                    <CModalBody className="px-5">

                        <div className="row mt-1">
                            <div className="col-md-3 mt-1">
                                <span>Username</span>
                            </div>
                            <div className="col-md-9">
                                <input value={formData.username ? formData.username : ''} onChange={handleFormInput} className="form-control" name="username" />
                                <small className="text-danger"> <i>{errorMsg.username ? errorMsg.username : ''}</i> </small>
                            </div>
                        </div>
                        <div className="row mt-1">
                            <div className="col-md-3 mt-1">
                                <span>User Type</span>
                            </div>
                            <div className="col-md-9">
                                <Select options={dropdownUserType} value={userTypeValue} name="id_user_type" onChange={handleSelectUserType} />
                                <small className="text-danger"> <i>{errorMsg.id_user_type ? errorMsg.id_user_type : ''}</i> </small>
                            </div>
                        </div>
                        <section className={secPass ? 'd-none' : 'd-hide'}>
                            <div className="row mt-1">
                                <div className="col-md-12 text-right mt-3">
                                    <button className="btn btn-secondary btn-sm" onClick={() => { loadModalChPass() }}><FaKey className="mr-2 mt--04" />Change Password</button>
                                </div>
                            </div>
                        </section>

                        <section className={secPass ? 'd-show' : 'd-none'}>
                            <div className="row mt-1">
                                <div className="col-md-3 mt-1">
                                    <span>Password</span>
                                </div>
                                <div className="col-md-9">
                                    <input value={formData.password ? formData.password : ''} type="password" className="form-control" name='password' onChange={handleFormInput} />
                                    <small className="text-danger"> <i>{errorMsg.password ? errorMsg.password : ''}</i> </small>
                                </div>
                            </div>
                            <div className="row mt-1">
                                <div className="col-md-3 mt-1">
                                    <span>Confirm Password</span>
                                </div>
                                <div className="col-md-9">
                                    <input value={formData.confirm_password ? formData.confirm_password : ''} type="password" className="form-control" name="confirm_password" onChange={handleFormInput} />
                                    <small className="text-danger"> <i>{errorMsg.confirm_password ? errorMsg.confirm_password : ''}</i> </small>
                                </div>
                            </div>
                        </section>

                    </CModalBody>
                    <CModalFooter className="justify-content-center" style={{ borderTop: "none" }}>
                        <button type="button" className="btn btn-sm btn-success w-100-p" onClick={() => submitForm()}><FaPaperPlane className="mr-2" />Submit</button>
                        <button type="button" className="btn btn-sm btn-secondary w-100-p" onClick={() => showModalInput(!modalInput)}><FaBan className="mr-2" />Cancel</button>
                    </CModalFooter>

                </CModal>

                {/* modal change password */}
                <CModal show={modalChPass} onClose={() => showModalChPass(!modalChPass)}>
                    <CModalHeader style={{ borderBottom: "none" }} closeButton>
                    </CModalHeader>
                    <CModalBody>
                        <div className="row mb-3 mt--2">
                            <div className="col-md-12 text-center">
                                <small className="text-black-50">Username</small>
                            </div>
                            <div className="col-md-12 text-center" style={{ marginTop: '-5px' }}>
                                <h4>{formData.username ? formData.username : '-'}</h4>
                            </div>
                        </div>
                        <hr />
                        <div className="row mt-1">
                            <div className="col-md-4 mt-1">
                                <span>Password</span>
                            </div>
                            <div className="col-md-8">
                                <input value={formData.password ? formData.password : ''} type="password" className="form-control" name='password' onChange={handleFormInput} />
                                <small className="text-danger"> <i>{errorMsg.password ? errorMsg.password : ''}</i> </small>
                            </div>
                        </div>
                        <div className="row mt-1">
                            <div className="col-md-4 mt-1">
                                <span>Confirm Password</span>
                            </div>
                            <div className="col-md-8">
                                <input value={formData.confirm_password ? formData.confirm_password : ''} type="password" className="form-control" name="confirm_password" onChange={handleFormInput} />
                                <small className="text-danger"> <i>{errorMsg.confirm_password ? errorMsg.confirm_password : ''}</i> </small>
                            </div>
                        </div>
                    </CModalBody>
                    <CModalFooter className="justify-content-center" style={{ borderTop: "none" }}>
                        <button type="button" className="btn btn-sm btn-success w-100-p" onClick={() => submitChPass()}><FaKey className="mr-2 mt--04" />Change</button>
                        <button type="button" className="btn btn-sm btn-secondary w-100-p" onClick={() => showModalChPass(!modalChPass)}><FaBan className="mr-2 mt--04" />Cancel</button>
                    </CModalFooter>
                </CModal>

                {/* modal delete */}
                <CModal show={modalDelete} onClose={() => showModalDelete(!modalDelete)}>
                    <CModalHeader style={{ borderBottom: "none" }} closeButton>
                    </CModalHeader>
                    <CModalBody>
                        <div className="row mb-3">
                            <div className="col-md-12 mt-1 text-center">
                                <span>Delete <b>{modalDelMsg}</b> ?</span>
                            </div>
                        </div>
                    </CModalBody>
                    <CModalFooter className="justify-content-center" style={{ borderTop: "none" }}>
                        <button type="button" className="btn btn-sm btn-secondary w-100-p" onClick={() => deleteData()}><FaTrash className="mr-2 mt--04" />Delete</button>
                        <button type="button" className="btn btn-sm btn-success w-100-p" onClick={() => showModalDelete(!modalDelete)}><FaBan className="mr-2 mt--04" />Cancel</button>
                    </CModalFooter>
                </CModal>

            </div>
        </>
    )
}

export default UserManagement
