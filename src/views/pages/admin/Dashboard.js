import React, { useState, useEffect } from 'react'
import { FcViewDetails, FcCalendar, FcAlarmClock, FcComboChart } from "react-icons/fc"
import axios from 'axios'

const Dashboard = () => {

  const [jadwalSholat, setjadwalSholat] = useState([])
  const [city, setCity] = useState('')
  const [date, setDate] = useState('')

  const getJadwalSholat = () => {

    axios.get('https://api.pray.zone/v2/times/today.json?city=bandung')
    .then(function (data) {
      setDate(data.data.results.datetime[0].date.gregorian + ' / ' + data.data.results.datetime[0].date.hijri)
      setCity(data.data.results.location.city)
      setjadwalSholat(data.data.results.datetime[0].times)
    })
    .catch(function (error) {
      console.log(error)
    })
    
  }

  useEffect(() => {
    getJadwalSholat()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  return (
    <>
      <div className="row justify-content-center mt-5">
        <div className="col-md-10">

          <div className="row">
            
            <div className="col-md-4 px-2">
              <div className="card rounded-2 shadow p-3 bg-primary-color-light-gradient">
                <div className="card-body">

                <div className="row">
                  <div className="col-md-12">
                    <h5 className="font-weight-bold"> Jadwal Sholat {city} </h5>
                    <small className="mt--2 text-white-50">{date}</small>
                  </div>
                </div>

                <hr className="line-black-50"/>

                <div className="row">
                  
                  <div className="col-md-12">
                    <div className="row">
                      
                      {
                        Object.keys(jadwalSholat).map((v,k) => {
                          return(
                            <div key={k} className="col-md-6 my-1">
                              <span>{v}</span>
                              <h6 className="font-weight-bold">{jadwalSholat[v]}</h6>
                            </div>
                          )
                        })
                      }
                      
                    </div>
                  </div>
                  
                </div>

                </div>
              </div>
            </div>

            <div className="col-md-3 px-2">
              <div className="card rounded-2 shadow p-3">
                <div className="card-body">
                  <div className="row">
                    <div className="col text-center">
                      <FcViewDetails style={{ fontSize: '55px' }}/>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card rounded-2 shadow p-3">
                <div className="card-body">
                  <div className="row">
                    <div className="col text-center">
                      <FcCalendar style={{ fontSize: '55px' }}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-md-3 px-2">
              <div className="card rounded-2 shadow p-3">
                <div className="card-body">
                  <div className="row">
                    <div className="col text-center">
                      <FcAlarmClock style={{ fontSize: '55px' }}/>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card rounded-2 shadow p-3">
                <div className="card-body">
                  <div className="row">
                    <div className="col text-center">
                      <FcComboChart style={{ fontSize: '55px' }}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
    </>
  )
}

export default Dashboard
