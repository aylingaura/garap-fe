import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import axios from 'axios'
import CIcon from '@coreui/icons-react'
import Loader from '../../../components/Loader_.js'

class Login extends Component {

  constructor() {
    super()
    this.state = {
      username: null,
      password: null,
      errorMessage: null,
      loader: false
    }
  }

  componentWillUnmount() {
    this.setState = (state, callback) => {
      return;
    };
  }

  render() {

    const SubmitLogin = () => {

      console.log(process.env.REACT_APP_ROOT_URL + 'login')

      const url = process.env.REACT_APP_ROOT_URL + 'login'
      const data = {
        'username': this.state.username,
        'password': this.state.password,
      }
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      }

      // perform a task before the request is sent
      axios.interceptors.request.use(config => {
        this.setState({ loader: true })
        return config;
      }, error => {
        return Promise.reject(error);
      });

      axios.post(url, data, config)
        .then(res => {
          localStorage.setItem('access_token', res.data.data.access_token)
          localStorage.setItem('expired_token_date', res.data.data.expired_token_date)
          localStorage.setItem('id_user_type', res.data.data.id_user_type)
          localStorage.setItem('user', JSON.stringify(res.data.data.detail_user))

          window.location.reload()
        })
        .catch(error => {
          this.setState({ errorMessage: error.response.data.message })
        })
        .then(() => {
          this.setState({ loader: false })
        })

    }

    // redirect to dashboard if login
    if (localStorage.getItem('access_token')) {
      return <Redirect to="/" />
    }
    else {
      return (
        <>
          <Loader isActive={this.state.loader} />

          <div className="c-app c-default-layout flex-row align-items-center">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-md-4">
                  <div className="card-group">

                    <div className="card p-4">
                      <div className="card-body">

                        <p className="text-danger">{this.state.errorMessage}</p>

                        <div className="input-group mb-1">
                          <div className="input-group-prepend">
                            <div className="input-group-text">
                              <CIcon name="cil-user" />
                            </div>
                          </div>
                          <input className="form-control" type="text" placeholder="Username" autoComplete="username" onChange={(e) => {
                            this.setState({ username: e.target.value })
                          }} />
                        </div>

                        <div className="input-group mb-4">
                          <div className="input-group-prepend">
                            <div className="input-group-text">
                              <CIcon name="cil-lock-locked" />
                            </div>
                          </div>
                          <input className="form-control" type="password" placeholder="Password" autoComplete="current-password" onChange={(e) => {
                            this.setState({ password: e.target.value })
                          }} />
                        </div>

                        <div className="row">
                          <div className="col-md-12 text-right">
                            <button className="btn btn-outline-dark px-4" onClick={SubmitLogin}>Login</button>
                          </div>
                        </div>

                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      );
    }

  }
}


export default Login;
