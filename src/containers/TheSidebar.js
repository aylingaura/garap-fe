import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from '@coreui/react'

import { AiFillSmile } from "react-icons/ai";

// sidebar nav config
import adminNav from './nav/admin_nav'
import developerNav from './nav/developer_nav'

const TheSidebar = () => {

  const [data, changeData] = useState(localStorage.getItem('id_user_type'))
  const dispatch = useDispatch()
  const show = useSelector(state => state.sidebarShow)

  return (
    <CSidebar show={show} onShowChange={(val) => dispatch({ type: 'set', sidebarShow: val })} >

      <CSidebarBrand className="d-md-down-none bg-primary-color" to="/">
        <AiFillSmile className="c-sidebar-brand-full text-white-50" style={{ fontSize: 30 }} />
        <AiFillSmile className="c-sidebar-brand-minimized text-white-50" style={{ fontSize: 30 }} />
      </CSidebarBrand>

      <CSidebarNav className="bg-primary-color pt-2">
        <CCreateElement
          items={
            data == '1' ? adminNav :
              data == '2' ? developerNav :
                adminNav
          }
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
        />
      </CSidebarNav>

      <CSidebarMinimizer className="c-d-md-down-none bg-primary-color" />

    </CSidebar>
  )
}

export default React.memo(TheSidebar)
