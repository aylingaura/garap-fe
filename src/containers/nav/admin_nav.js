const _nav = [

  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/admin/dashboard',
    icon: 'cilCircle'
  },

  {
    _tag: 'CSidebarNavTitle',
    _children: ['Master']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'User Type',
    to: '/user-management/user',
    icon: 'cil-user',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Status',
    to: '/user-management/user',
    icon: 'cil-user',
  },


  {
    _tag: 'CSidebarNavTitle',
    _children: ['User Management']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'User',
    to: '/user-management/user',
    icon: 'cil-user',
  },

]

export default _nav
