import { createStore } from 'redux'

const initialState = {
  sidebarShow: 'responsive',
  accessToken: '',
  headers: {
    headers: {
      'Authorization': 'Bearer ' + localStorage.getItem("access_token")
    }
  }
}

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case 'set':
      return { ...state, ...rest }
    case 'SET_LOGIN_TOKEN':
      return { ...state, accessToken: rest.accessToken }
    default:
      return state
  }
}

const store = createStore(changeState)
export default store